require 'net/http'
require 'json'

class ResultSender
  attr_reader :revision_name
  DEFAULT_PORT = 3000

  def initialize(revision_name="result_for_#{Time.now.to_i}", app_port=DEFAULT_PORT)
    @revision_name = revision_name
    @all_port = app_port
  end

  def send_revision
    data = { revision: @revision_name }
    send_json(data, 'revisions')
  end

  def send_revision_complete
    data = { revision: @revision_name, completed: 1 }
    send_json(data, 'revisions')
  end

  def send_test(scenario)
    scenario_error = scenario.exception.message if scenario.exception
    run_path = "cucumber #{scenario.location.file}:#{scenario.location.line}"
    data = { name: scenario.name,
             status: scenario.status,
             feature_name: scenario.feature.name,
             run_path: run_path,
             steps: scenario.location.filepath.filename,
             error: scenario_error,
             revision_name: @revision_name
    }
    send_json(data, 'tests')
  end

  def send_json(body, path)
    @host = 'localhost'
    @port = @all_port

    @path = "/api/#{path}"
    @body = body.to_json

    request = Net::HTTP::Post.new(@path, initheader = {'Content-Type' =>'application/json'})
    request.body = @body
    response = Net::HTTP.new(@host, @port).start {|http| http.request(request) }
    puts "Response #{response.code} #{response.message}: #{response.body}"
  end
end