Feature: Google Search

  Scenario: Open Wiki page
    Given Search Page is open
    When I type a search key and press Enter button
    And Open Wiki page from the search result
    #Then I should see Wiki which includes the search key

  Scenario: Open non-wiki page
    Given Search Page is open
    When I type a search key and press Enter button
    And Open any page except wiki
    #Then I should see a page with the search key displayed