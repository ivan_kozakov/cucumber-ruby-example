require 'capybara'
require 'capybara/cucumber'
require 'rspec'
require 'site_prism'
require 'pry'

Capybara.configure do |capybara|
  capybara.default_driver = :selenium
  capybara.app_host = 'https://google.com'
end
