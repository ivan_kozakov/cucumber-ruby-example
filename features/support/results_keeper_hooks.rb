if ENV['SEND_RK_RESULTS']
  require 'results_keeper'

  ResultsKeeper.instance.send_revision

  After do |scenario|
    ResultsKeeper.instance.send_test(scenario)
  end

  Before do |scenario|
    ResultsKeeper.instance.test_started(scenario)
  end

  at_exit do
    ResultsKeeper.instance.send_revision_complete
  end
end