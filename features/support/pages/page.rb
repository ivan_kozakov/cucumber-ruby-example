class Page < SitePrism::Page

  def safe_click(selector)
    safe_find(selector).click
  end

  def safe_set(selector, value)
    safe_find(selector).set(value)
  end
end