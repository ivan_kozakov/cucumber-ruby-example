class SearchResultsPage < SitePrism::Page

  elements :search_results, '.r a'
  WIKIPEDIA_MAIN_HOST = 'wikipedia.org'

  def search_result_links
    search_results.map{|result| result[:href] }
  end

  def wiki_url?(url)
    url.include? WIKIPEDIA_MAIN_HOST
  end

  def wiki_link
    search_result_links.find{|url| wiki_url?(url) }
  end

  def non_wiki_link
    search_result_links.find{|url| !wiki_url?(url) }
  end
end