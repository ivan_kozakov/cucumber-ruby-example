class HomePage < SitePrism::Page

  set_url '/'
  set_url_matcher /google.com\/?/

  element :search_field, 'input[name=\'q\']'
  element :search_button, 'button[name=\'btnG\']'

  def search_for(search_key)
    search_field.set(search_key)
    search_button.click
  end

end