module MyCustom
  class Formatter
    def initialize(runtime, io, options)
      @io = io
    end

    def before_test_case(test_case)
      feature = test_case.source.first
      scenario = test_case.source.last
      @io.puts feature.short_name.upcase
      @io.puts "  #{scenario.name.upcase}"
    end

    def after_test_case(test_case)
      feature = test_case.source.first
      scenario = test_case.source.last
      @io.puts feature.short_name.upcase
      @io.puts "  #{scenario.name.upcase}"
    end
  end
end