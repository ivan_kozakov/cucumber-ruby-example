Given(/^Search Page is open$/) do
  @search_key = 'Aikido'

  @home_page = HomePage.new
  @home_page.load
  expect(123).to eq(321)
end

When(/^I type a search key and press Enter button$/) do
  @home_page.search_for(@search_key)
  sleep 1
end

And(/^Open Wiki page from the search result$/) do
  @search_results_page = SearchResultsPage.new
  expect(2).to eq(3)
  #visit @search_results_page.wiki_link
  end

And(/^Open any page except wiki$/) do
  @search_results_page = SearchResultsPage.new
  visit @search_results_page.non_wiki_link
  expect(2).to eq(3)
end

Then(/^I should see Wiki which includes the search key$/) do
  page.should have_content('From Wikipedia, the free encyclopedia')
  page.should have_content(@search_key)
  end

Then(/^I should see a page with the search key displayed$/) do
  page.should have_no_content('wiki')
  page.should have_content(@search_key)
end


When(/^User is logged to the system$/) do
  puts '2'
end

When(/^Opens Users tab$/) do
  puts ''
end

When(/^Clicks "([^"]*)" button$/) do |arg1|
  puts ''
end

When(/^Fills in all required fields$/) do
  puts ''
end

When(/^Click "([^"]*)" button$/) do |arg1|
  puts ''
end

Then(/^"([^"]*)" message is displayed$/) do |message|
  expect(message).to include(' ')
end

When(/^Selects a user and clicks "([^"]*)" button$/) do |arg1|
  puts ''
end

When(/^Confirms deleting$/) do
  puts ''
end

When(/^Clicks on users photo icon$/) do
  puts ''
end

When(/^Attaches a new file$/) do
  puts ''
end

Then(/^The new photo has appropriate src$/) do
  puts ''
end

When(/^Clicks on "([^"]*)" button$/) do |arg1|
  puts ''
end

When(/^Types the current password and new ones$/) do
  puts ''
end

Then(/^User is logged out$/) do
  puts ''
end

When(/^Updates user name$/) do
  puts ''
end

When(/^Inputs a new user email$/) do
  puts ''
end

When(/^Changes permissions to an admin$/) do
  puts ''
end