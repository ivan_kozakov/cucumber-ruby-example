Feature: Users

  Scenario: Create a new user
    When User is logged to the system
    And Opens Users tab
    And Clicks "Create New User" button
    And Fills in all required fields
    And Click "Create" button
    Then "User has been created successfully" message is displayed

  Scenario: Delete a user
    When User is logged to the system
    And Opens Users tab
    And Selects a user and clicks "Delete" button
    And Confirms deleting
    Then "User has been deleted successfully" message is displayed

  Scenario: Update user's profile photo
    When User is logged to the system
    And Opens Users tab
    And Selects a user and clicks "Update" button
    And Clicks on users photo icon
    And Attaches a new file
    And Clicks "Update" button
    Then "User profile has been updated successfully" message is displayed
    Then The new photo has appropriate src

  Scenario: Change user password
    When User is logged to the system
    And Opens Users tab
    And Selects a user and clicks "Update" button
    And Clicks on "Change Password" button
    And Types the current password and new ones
    And Clicks "Change Now" button
    Then "User profile has been updated successfully" message is displayed
    Then User is logged out

  Scenario: Edit the user profile
    When User is logged to the system
    And Opens Users tab
    And Selects a user and clicks "Update" button
    And Updates user name
    And Clicks "Update" button
    Then "User profile has been updated successfully" message is displayed

  Scenario: Invite a new user
    When User is logged to the system
    And Opens Users tab
    And Clicks "Invite" button
    And Inputs a new user email
    And Clicks "Update" button
    Then "User profile has been updated successfully" message is displayed

  Scenario: Change user permissions (create an admin user)
    When User is logged to the system
    And Opens Users tab
    And Selects a user and clicks "Update" button
    And Changes permissions to an admin
    And Clicks "Update" button
    Then "User profile has been updated successfully" message is displayed


